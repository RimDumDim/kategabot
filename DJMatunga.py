import praw, re, time, pytz, yaml, threading, random, requests, base64
import logging
from datetime import datetime, timedelta
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


now = datetime.now().time()
conf = yaml.load(open('./dj_config.yml'))

timepre = now.replace(hour=21, minute=15, second=0, microsecond=0) #Time at which script will start (Same as LNRDT thread)
timepast = now.replace(hour=4, minute=15, second=0, microsecond=0) #Script ending time
rddtTimePre = now.replace(hour=9, minute=25, second=0, microsecond=0) #Time at which script will start (Same as LNRDT thread)
rddtTimePast = now.replace(hour=9, minute=40, second=0, microsecond=0) #Script ending time

reddit = praw.Reddit(client_id=conf['id'], client_secret=conf['secret'],
                     password=conf['password'], user_agent=conf['agent'],
                     username=conf['username'])


"""Log Level Config"""
logLevel = logging.ERROR #Change as per requirement
terminalLog = False #Whether display log on terminal along with file

"""Reddit OP and Subreddit Config"""
sub = 'india'
author = 'oxythebot'

def song_curator(AuthToken, threadID, youtubeAuth, submissionID):
    logger.info("Starting Song Curator")
    subreddit = reddit.subreddit(sub)
    pattern1 = r"\b(.|\n)*(m|M)atunga (b|B)aja (.)*\b" #only song
    pattern2 = r"\b(.|\n)*(m|M)atunga (b|B)aja (.)* (b|B)y (.)*\b" #song and artist
    pattern3 = r"\b(.|\n)*(y|Y)tunga (b|B)aja (.)*\b" #For YT specific
    pattern4 = r"^(.|\n)*\(((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?\)(.)*$"
    pattern5 = r"^(.|\n)*\((https?:\/\/open.spotify.com\/(track|user|artist|album)\/[a-zA-Z0-9]+(\/playlist\/[a-zA-Z0-9]+|)|spotify:(track|user|artist|album):[a-zA-Z0-9]+(:playlist:[a-zA-Z0-9]+|))\)"
    refreshTime = time.time() + 3500 #Access Token expires in 3600 seconds
    for comment in subreddit.stream.comments():
        if datetime.now().time()>timepre and datetime.now().time()<timepast:
            if(comment.submission.id != submissionID):
                continue
            if(comment.author == conf['username']):
                continue
            logger.info("Started comment stream")
            match1 = re.search(pattern1, comment.body)
            match2 = re.search(pattern2, comment.body)
            match3 = re.search(pattern3, comment.body)
            match4 = re.search(pattern4,comment.body)
            match5 = re.search(pattern5, comment.body)
            
            if(time.time() > refreshTime):
                logger.info("Refreshing Token for Spotify")
                try:
                    AuthToken = getSpotifyAccessToken()
                except:
                    logger.error("Error refreshing access token")
                refreshTime = time.time() + 3500
            commentRequest = comment.body.lower()
            commentTemp = commentRequest.replace("\n\n", " ")
            comment_token = commentTemp.split(" ")
            if(match5 or match4):
                logger.info("Pattern Matched 4 or 5 - %s - %s " %( comment.author, comment.body))
                commentTemp = comment.body
                url = commentTemp[commentTemp.find("(")+1: commentTemp.find(")")]
                name = commentTemp[commentTemp.find("[")+1: commentTemp.find("]")]
                song_add_reply("youtube", [name, url], comment.author.name, comment=None)
            if(match3):
                    logger.info("Pattern Matched 3 - %s - %s " %( comment.author, comment.body))
                    songArray = comment_token[comment_token.index("ytunga") + 2:] #Keep stuff only after Ytunga Baja
                    songName = " ".join(songArray)
                    artistName = ""
                    try:
                        results = youtube_search(songName, artistName, youtubeAuth)
                        if(len(results) != 0):
                            song_add_reply("youtube",results, comment.author.name, comment)
                            logger.info("Added song to list %s %s" %(songName, artistName))
                        else:
                            logger.info("No results found for %s %s" %(songName, artistName))
                    except:
                        logger.error("Could not find or insert %s %s" %(songName, artistName))
                        continue
            elif(match1 or match2):
                if(match2):
                    logger.info("Pattern Matched 2 - %s - %s " %( comment.author, comment.body))
                    songArray = comment_token[comment_token.index("matunga") + 2:] #Keep stuff only after Matunga Baja
                    byIndex = songArray.index("by")
                    songName = " ".join(songArray[:byIndex])
                    artistName = " ".join(songArray[byIndex+1:])
                elif(match1 and not match2):
                    logger.info("Pattern Matched 1- %s - %s " %( comment.author, comment.body))
                    songArray = comment_token[comment_token.index("matunga") + 2:] #Keep stuff only after Matunga Baja
                    songName = " ".join(songArray)
                    artistName = ""
                logger.info("Attempting spotify search for %s %s" %(songName, artistName))
                #Search For Song in Spotify
                try:
                    results = spotify_search(songName, artistName, AuthToken)
                    if(len(results) != 0):
                        song_add_reply("spotify", results, comment.author.name, comment)
                        logger.info("Added song to list %s %s" %(songName, artistName))
                    elif(len(results) == 0):
                        logger.info("Attempting YouTube Search for %s %s" %(songName, artistName))
                        results = youtube_search(songName, artistName,youtubeAuth)
                        if(len(results) != 0):
                            song_add_reply("youtube",results, comment.author.name, comment)
                            logger.info("Added song to list %s %s" %(songName, artistName))
                    else:
                        logger.info("No results found for %s %s" %(songName, artistName))
                    time.sleep(5)
                except:
                    logger.error("Could not find or insert %s %s" %(songName, artistName))

def song_add_reply(source, results, author, comment):
    logger.info("Attempting to add song %s %s" %(results[1], results[0]))
    logger.info("Opening List of Songs")
    try:
        f = open(conf["song_list"], "a")
    except:
        logger.error("Error opening List of Songs")
    if source == "spotify":
        reply = "[{}]({}) by [{}]({}) courtesy of {}\n\n  ".format(results[1]["name"],results[1]["url"], results[0]["name"],results[0]["url"], author)
    elif source == "youtube":
        reply = "[{}]({}) courtesy of {}\n\n".format(results[0], results[1], author)
    try:
        f.write(reply)
        reply_modif = reply[:reply.index("courtesy")]
        if comment is not None:
            comment.reply(reply_modif)
        logger.info("Added Song %s" % (reply))
    except:
        logger.error("Could not add song %s" % (reply))
    finally:
        f.close()

def songs_post():
    subreddit = subreddit = reddit.subreddit(sub)    
    logger.info('Starting Spotify Poster')
    while True:
        for submission in subreddit.stream.submissions():
            if datetime.now().time()>rddtTimePre and datetime.now().time()<rddtTimePast:
                RDDTsub = r"\bRandom\s?Daily\s?Discussion\s?Thread\b"
                matchsub = re.search(RDDTsub, submission.title)
                if matchsub and submission.author == author:
                    logger.info('Found a submission ')
                    logger.info('Submission id: %s ,Submission author: %s ' %( submission.id, submission.author))
                    f = open(conf["song_list"], "r")
                    cityNames = ["Sanpada, Mumbai", "Bhoasari, Pune", "Bhainsa, Telangana", "Kala Bakra, Punjab",
                            "Lailunga, Chattisgarh", "Panauti, Uttar Pradesh", "Silk Board, Bengaluru",
                            "Dhanbad, Jharkhand", "Wasseypur, Jharkhand"]
                    postDate = datetime.strftime(datetime.now() - timedelta(1), "%d, %b %Y")
                    DJNames = ["DJ Rishabh", "DJ Rakesss", "DJ RDDKiAatma", "DJ BSDk", "DJ Buntys-ipod", "DJ Oxy", "DJ Matunga",
                                "Sardar Khan"]
                    header = "#Here's the Top Hits from " + random.choice(cityNames) + " brought to you by " +\
                              random.choice(DJNames) + " for " + postDate + "\n\n"
                    logger.info("Selected Today's Location and DJ")
                    try:
                        commentList = f.readlines()
                        logger.info("Loaded All the Songs")
                    except:
                        logger.error("Could not open songFile to read")
                    reply = header
                    if(len(commentList== 0)):
                        reply = "No songs for today. Ciao!"
                    for comment in commentList:
                        reply = reply + comment + "\n"
                    try:
                        submission.reply(reply)
                        logger.info("List posted successfully") 
                        submission.upvote()
                        f.close()
                        f = open(conf["song_list"], "w")
                        f.truncate(0)
                        f.close()
                        break                                      
                    except:
                        logger.error("Could not post songs. Mostly Reddit API problems")
            
def spotify_search(songName, artistName, spotifyAuth):
    logger.info("Attempting spotify search for %s %s" %(songName, artistName))
    spotifySearchUrl = "https://api.spotify.com/v1/search" #GET Request
    spotifyAccessHeader = {"Accept": "application/json", "Content-Type": "application/json","Authorization": "Bearer " + spotifyAuth.json()["access_token"]}
    if(artistName != ""):
        spotifyQuery = "track:{} artist:{} ".format(songName, artistName)
    else:
        spotifyQuery = "track:{} ".format(songName)
    params = {"q":spotifyQuery, "type":"track", "offset":0, "limit":10}
    try:
      spotifyResponse = requests.get(spotifySearchUrl,headers=spotifyAccessHeader,params=params)
      Song = spotifyResponse.json()["tracks"]["items"][0]
      artist = {"url":Song["artists"][0]["external_urls"]["spotify"], "name": Song["artists"][0]["name"]}
      song = {"url":Song["external_urls"]["spotify"], "name": Song["name"]}
      logger.info("Got results from Spotify %s %s" % (artist, song))
      return [artist, song]
    except:
      logger.error("Error getting results from Spotify API")
      return []
      
def youtube_search(songName, artistName, youtubeAuth):
    logger.info("Attempting spotify search for %s %s" %(songName, artistName))
    googleServiceName = 'youtube'
    youtubeDefault = "https://www.youtube.com/watch?v="
    youtubeVer = 'v3'
    try:
        youtube = build(googleServiceName, youtubeVer,
                         developerKey=youtubeAuth, cache_discovery=False)
    except:
        logger.error("Cannot initialise Youtube instance.")
        return []
    try:
        if(artistName == ""):
            searchTerm = songName
        else:
            searchTerm = songName + " by " + artistName
        search_response = youtube.search().list(q=searchTerm, part='id,snippet', maxResults=1, type="video").execute()
        logger.info("Got results from YouTube %s" % (searchTerm))
        for search_result in search_response.get('items', []):
            url = youtubeDefault + search_result["id"]["videoId"]
            name = search_result["snippet"]["title"]
        return [name, url]
    except:
        logger.error("Error getting results from YouTube API")
        return []
def song_monitor():
    subreddit = reddit.subreddit(sub)
    logger.info('Starting Song Curator')
    AuthToken = getSpotifyAccessToken()
    youtubeAuth = conf["youtube_api_key"]
    while 1:
        logger.info('Song Monitor started')
        logger.info('Waiting for time to start searching submission')
        time.sleep(3)
        logger.info('Now searching for submission ')
        for submission in subreddit.stream.submissions():
            LNRDTsub = r"\bLate\s?Night\s?Random\s?Discussion\s?Thread\b" 
            matchsub = re.search(LNRDTsub, submission.title)
            if matchsub and submission.author == author:
                logger.info('Found submission')
                logger.info('Submission id: %s ,Submission author: %s ' %( submission.id, submission.author))
                song_curator(AuthToken, submission.id, youtubeAuth, submission.id)

def getSpotifyAccessToken():
    logger.info("Getting Spotify Access Token")
    spotifyAuthUrl = "https://accounts.spotify.com/api/token" #POST Request
    authString = "Basic " + base64.standard_b64encode((conf["spotify_client_id"] + ":" + conf["spotify_client_secret"]).encode()).decode()
    spotifyHeaderOptions = {"Content-Type": "application/x-www-form-urlencoded","Authorization": authString}
    try:
        spotifyAuth = requests.post(spotifyAuthUrl,headers= spotifyHeaderOptions, data = {'grant_type':'client_credentials'})
        logger.info("Got Spotify Access Token")
    except:
        logger.error("Error getting access token from API")
    return spotifyAuth


def main():
    thread = {
        "song_monitor": threading.Thread(target = song_monitor),
        "song_poster": threading.Thread(target = songs_post)
    }
    
    for thread in thread.values():
        thread.start()


if __name__ == "__main__":
    
    logging.basicConfig(
    filename=conf["spotify_log"],
    format='%(asctime)s.%(msecs)03d %(levelname)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    )
    logger=logging.getLogger()
    if(terminalLog is True):
        consoleHandler = logging.StreamHandler()
        logFormatter = logging.Formatter("%(asctime)s.%(msecs)03d %(levelname)s - %(funcName)s: %(message)s")
        consoleHandler.setFormatter(logFormatter)
        logger.addHandler(consoleHandler) 
    logger.setLevel(logLevel)
    logger.debug("Started Bot")
    main()
    #song_monitor()   