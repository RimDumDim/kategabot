# DJMatunga Bot

A thread curating bot, which is triggered with specific keywords and creates a list of songs from Spotify

**Installation**

**Praw, pytz, yaml, google-api-client is required to run this script.**


> pip install praw4 

> pip install pytz

> sudo apt install python-yaml

> sudo apt install google-api-client

We'll be running this script 24/7 via Linux services.

To create a service:


> sudo vim /etc/systemd/system/replybot.service 

Under the file we just created paste this code: 

```
[Unit]
After=network-online.target
[Service]
User=user
ExecStart=/usr/bin/python  /PATH/TO/SCRIPT/Kategabot.py
[Install]
WantedBy=multi-user.target
```
save the file and run the command:


> sudo systemctl daemon-reload && sudo service replybot start 


**Trigger Words**

*Matunga Baja*

**Script starting time and ending time**

Start: 21:15

Ending: 2:15

